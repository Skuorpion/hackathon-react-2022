import useSessions from "../hooks/useSessions";
import { useState } from "react";
import { useContext } from "react";
import { UserContext } from "../App";
import { Navigate } from "react-router";
import { useAuth } from "../hooks/auth";

const Sessions = (props) => {
  const { filtres } = props;

  const [loginState, actions] = useAuth();
  const { user } = loginState;

  function reservation(sessionId) {
    const userId = user["id"];
    const url = "/api/sessions/" + sessionId;
    fetch(url, {
      method: "PATCH",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        studentId: userId,
      }),
    }).then((res) => {
      if (res.ok) {
        console.log(res.json());
      } else {
        throw new Error("Unable to reserve");
      }
    });
  }

  const { refresh, lastUpdate, data } = useSessions({
    minDate: Date.parse(filtres.minDate),
    favoritePlace: filtres.favoritePlace,
    favoriteInstructors: filtres.favoriteInstructors,
  });

  return (
    <div>
      {!user && useAuth.loading && <Navigate to="/login" />}

      {data && !!filtres.minDate && (
        <pre>
          <a href="/sessions/new">Ajouter une sessions</a>
          {data
            .filter((item) => item.studentId != null)
            .map((i) => (
              <li key={i.id}>
                {new Date(i.dateStart).toLocaleString()}{" "}
                {i.instructor.firstName} {i.instructor.lastName} {i.place.name}
                <button onClick={() => reservation(i.id)}>Reserver</button>
              </li>
            ))}
        </pre>
      )}
    </div>
  );
};

export default Sessions;
