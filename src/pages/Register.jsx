import React from "react";
import RegisterForm from "../components/RegisterForm";
import { Navigate } from "react-router";

function register(firstName, lastName, email, password, role) {
  /*console.log(
    firstName + " " + lastName + " " + email + " " + password + " " + role
  );*/
  fetch("/api/users", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      firstName: firstName,
      lastName: lastName,
      email: email,
      password: password,
      favoritePlacesId: [],
      favoriteInstructorsId: [],
      role: role,
    }),
  }).then((res) => {
    if (res.ok) {
      console.log(res.json());
    } else {
      throw new Error("Unable to register");
    }
  });
}

const handleSubmit = ({ firstName, lastName, email, password, role }) => {
  register(firstName, lastName, email, password, role);
};

const Register = () => {
  return (
    <div>
      <h2>Register Student</h2>
      <RegisterForm role="student" onSubmit={handleSubmit} />
      <h2>Register Moniteur</h2>
      <RegisterForm role="instructor" onSubmit={handleSubmit} />
    </div>
  );
};
export default Register;
