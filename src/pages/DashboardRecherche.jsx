import FavoriteInstructors from "../components/FavoriteInstructors";
import FavoritePlaces from "../components/FavoritePlaces";
import FilterDate from "../components/FilterDate";
import { useEffect, useContext, useState } from "react";
import { useAuth } from "../hooks/auth";
import Footer from "../parts/Footer";
import Sessions from "./Sessions";
import { Navigate } from "react-router";

const DashboardRecherche = () => {
  const [loginState] = useAuth(true);

  const user = loginState.user;

  const [filtres, setFiltres] = useState({
    minDate: null,
    favoriteInstructors: null,
    favoritePlace: null,
  });

  const handleChange = (name, value) => {
    setFiltres({
      ...filtres,
      [name]: value,
    });
  };

  return (
    <body class="eleves eleves--bleu">
      {!user && useAuth.loading && <Navigate to="/login" />}
      {user && <h2 class="eleves__name">Bonjour {user.firstName}</h2>}
      <section class="form--recherche">
        <form action="" method="get" class="form-recherche">
          <h1 class="form-recherche__title">
            Trouve ta prochaine heure de conduite
          </h1>
          <div class="form-connexion-date">
            <FilterDate
              onChange={(value) => handleChange("minDate", value)}
              value={filtres.minDate}
            />
          </div>
          <div class="form-connexion-sceance">
            <FavoriteInstructors
              onSelect={(value) => handleChange("favoriteInstructors", value)}
              value={filtres.favoriteInstructors}
            />
          </div>
          <div class="form-connexion-sceance">
            <FavoritePlaces
              onSelect={(value) => handleChange("favoritePlace", value)}
              value={filtres.favoritePlace}
            />
          </div>
          <div class="form-btn">
            <input type="submit" value="Rechercher" />
          </div>
        </form>
      </section>
      <Sessions filtres={filtres} />
      <Footer />
    </body>
  );
};

export default DashboardRecherche;
