import useSessions from "../hooks/useSessions";
import { useState } from "react";

const FilterDate = (props) => {
  /*const [filtres, setFiltres] = useState({
    minDate: "",
  }); 

  const handleChange = (e) => {
    console.log(e.target.name)
    setFiltres({
      ...filtres,
      [e.target.name]: e.target.value,
    })
  }*/

  const { onChange, value } = props;




  return (
    <div>
      <input
        type="date"
        onChange={(e) => onChange(e.target.value)}
        name="minDate"
        value={value}
      ></input>
    </div>
  );
};

export default FilterDate;
