import { useState } from "react";
import { useNavigate } from "react-router-dom";
import { Navigate } from "react-router";
import { useAuth } from "../hooks/auth";

const Credits = () => {
  const navigate = useNavigate();

  const [selectedOffer, setSelectedOffer] = useState({
    amount: 0,
    price: 0,
  });

  const [loginState, actions] = useAuth();
  const { user } = loginState;

  const offers = [
    {
      amount: 1,
      price: 35,
    },
    {
      amount: 5,
      price: 190,
    },
    {
      amount: 20,
      price: 650,
    },
  ];

  const handleCheck = (offer) => {
    setSelectedOffer(offer);
  };

  const submitOffer = (offer) => {
    if (offer) {
      fetch(`/api/students/${user.id}`, {
        method: "PATCH",
        headers: {
          "Content-Type": "Application/json",
        },
        body: JSON.stringify({
          credits: user.credits + selectedOffer.amount,
        }),
      }).then((res) => res.json());

      navigate("/dashboard");
    }
  };

  return (
    <main>
      {!user && useAuth.loading && <Navigate to="/login" />}
      <h2>Ajouter des crédits</h2>
      <ul>
        {offers.map((offer, i) => {
          return (
            <li key={i}>
              <h3>
                {offer.ammout} crédit{offer.ammout > 1 ? "s" : ""} -{" "}
                {offer.price} €
              </h3>
              <input
                type="radio"
                checked={selectedOffer.price === offer.price}
                onChange={() => handleCheck(offer)}
                value={i}
                name=""
                id=""
              />
            </li>
          );
        })}
      </ul>
      <input
        type="button"
        disabled={selectedOffer.price === 0}
        onClick={() => submitOffer(selectedOffer)}
        value={`payer ${
          selectedOffer.price === 0 ? "" : selectedOffer.price + "€"
        }`}
      />
    </main>
  );
};

export default Credits;
