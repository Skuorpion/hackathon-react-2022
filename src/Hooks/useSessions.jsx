import { useCallback, useEffect, useState } from "react"

const useSessions = (settings) => {

  const{
    minDate,
    favoritePlace,
    favoriteInstructors
    } = settings

  const[session, setSession] = useState({data: null, lastUpdate: null});

  console.log(minDate)

  const refresh = useCallback( () => {
    const seachParams = new URLSearchParams();
    seachParams.append("_expand","instructor")
    seachParams.append("_expand","place")
  
    if(minDate){
      seachParams.append("dateStart_gte", minDate);
    }

    if(typeof favoriteInstructors == 'number'){
      seachParams.append("instructorId", favoriteInstructors);
    }

    if(typeof favoritePlace == 'number'){
      seachParams.append("placeId", favoritePlace);
    }
   console.log(favoritePlace)
    
    fetch("/api/sessions?" + seachParams.toString())
    .then(res => res.json())
        .then(data => {
          setSession({data, lastUpdate: Date.now()})
          })
        },[minDate, favoritePlace, favoriteInstructors]);
  
  useEffect( () => {
    refresh();
  }, [refresh])
  
  return {
    ...session,
    refresh
  }

}

export default useSessions;